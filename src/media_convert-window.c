/* media_convert-window.c
 *
 * Copyright 2022 Stephan Vedder
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "media_convert-config.h"
#include "media_convert-window.h"

#include <glib/gi18n.h>

struct _MediaConvertWindow
{
  GtkApplicationWindow  parent_instance;

  /* Template widgets */
  GtkHeaderBar        *header_bar;
  GtkListView         *pending_list;
};

G_DEFINE_TYPE (MediaConvertWindow, media_convert_window, GTK_TYPE_APPLICATION_WINDOW)

static void
on_response(GtkNativeDialog *native,
             int              response)
{
  if (response == GTK_RESPONSE_ACCEPT)
    {
      GtkFileChooser *chooser = GTK_FILE_CHOOSER (native);
      GFile *file = gtk_file_chooser_get_file (chooser);

      

      g_object_unref (file);
    }

  g_object_unref (native);
}

static void
media_convert_window_add_media (GSimpleAction *action,
                                GVariant      *parameter,
                                gpointer       user_data)
{
  MediaConvertWindow *self = MEDIA_CONVERT_WINDOW (user_data);

  GtkFileChooserNative *native;

  native = gtk_file_chooser_native_new (_("Choose media"),
                                        GTK_WINDOW(self),
                                        GTK_FILE_CHOOSER_ACTION_OPEN,
                                        "_Save",
                                        "_Cancel");

  g_signal_connect (native, "response", G_CALLBACK (on_response), NULL);
  gtk_native_dialog_show (GTK_NATIVE_DIALOG (native));
}

static void
media_convert_window_class_init (MediaConvertWindowClass *klass)
{
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  gtk_widget_class_set_template_from_resource (widget_class, "/org/feliwir/MediaConvert/media_convert-window.ui");
  gtk_widget_class_bind_template_child (widget_class, MediaConvertWindow, header_bar);
  gtk_widget_class_bind_template_child (widget_class, MediaConvertWindow, pending_list);
  
  gtk_widget_class_add_binding_action (widget_class, GDK_KEY_o, GDK_CONTROL_MASK, "win.add-media", NULL);
}

static void
media_convert_window_init (MediaConvertWindow *self)
{
  gtk_widget_init_template (GTK_WIDGET (self));

  g_autoptr (GSimpleAction) add_media_action = g_simple_action_new ("add-media", NULL);
  g_signal_connect (add_media_action, "activate", G_CALLBACK (media_convert_window_add_media), self);
  g_action_map_add_action (G_ACTION_MAP (self), G_ACTION (add_media_action));
}
